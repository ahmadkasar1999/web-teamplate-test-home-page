import React from 'react';
import Header from '../components/Header/Header';
import { Grid } from 'semantic-ui-react';
import Footer from '../components/Footer/Footer';
import Services from '../components/Services/Services';
import Carousel from '../components/Carousel/Carousel';
import Domain from '../components/Domain/Domain';
import AboutUs from '../components/AboutUs/AboutUs';
export default function HomePage() {
  return (
    <Grid>
      <Grid.Row>
        <Grid.Column>
          <Header />
          <Domain />
          <AboutUs />
          <Carousel />
          <Services />
          <Footer />
        </Grid.Column>
      </Grid.Row>
    </Grid>
  );
}
