import React from 'react';
import { Container, Grid, Image, Icon } from 'semantic-ui-react';
import './style.css';
export default function AboutUs() {
  return (
    <Container style={{ borderTop: '1px solid rgb(235 231 231)', marginTop: '60px' }}>
      <Grid style={{ paddingTop: '80px' }}>
        <Grid.Row>
          <Grid.Column mobile={16} tablet={16} computer={6} className='border-right m-bottom-small'>
            <div>
              <h2 className='m-b-5-title'>About Us</h2>
              <p className='color-gry p-right-parag '>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad Lorem ipsum dolor sit amet, consectetur
                adipiscing elit, sed do eiusmod tempor incididunt
              </p>
            </div>
          </Grid.Column>
          <Grid.Column mobile={16} tablet={16} computer={10}>
            <Grid>
              <Grid.Row style={{ justifyContent: 'center' }}>
                <Grid.Column mobile={16} tablet={16} computer={5} textAlign='center' verticalAlign='center' style={{ display: 'flex', justifyContent: 'center' }}>
                  <div className='m-bottom-box-image'>
                    <Image src='https://react.semantic-ui.com/images/wireframe/square-image.png' circular style={{ width: '80px' }} />
                    <h3 className='m-b-5-title'>Jonny Doe</h3>
                    <p className='color-gry '>Designer</p>
                    <div>
                      <span className='color-gry '>
                        <Icon name='facebook f' />
                      </span>
                      <span style={{ marginLeft: '7px', marginRight: '7px' }} className='color-gry '>
                        <Icon name='facebook f' />
                      </span>
                      <span className='color-gry '>
                        <Icon name='facebook f' />
                      </span>
                    </div>
                  </div>
                </Grid.Column>
                <Grid.Column mobile={16} tablet={16} computer={5} textAlign='center' verticalAlign='center' style={{ display: 'flex', justifyContent: 'center' }}>
                  <div className='m-bottom-box-image'>
                    <Image src='https://react.semantic-ui.com/images/wireframe/square-image.png' style={{ width: '80px' }} circular />
                    <h3 className='m-b-5-title'>Jonny Doe</h3>
                    <p className='color-gry '>Designer</p>
                    <div>
                      <span className='color-gry '>
                        <Icon name='facebook f' />
                      </span>
                      <span style={{ marginLeft: '7px', marginRight: '7px' }} className='color-gry '>
                        <Icon name='facebook f' />
                      </span>
                      <span className='color-gry '>
                        <Icon name='facebook f' />
                      </span>
                    </div>
                  </div>
                </Grid.Column>
                <Grid.Column mobile={16} tablet={16} computer={5} textAlign='center' verticalAlign='center' style={{ display: 'flex', justifyContent: 'center' }}>
                  <div className='m-bottom-box-image'>
                    <Image src='https://react.semantic-ui.com/images/wireframe/square-image.png' style={{ width: '80px' }} circular />
                    <h3 className='m-b-5-title'>Jonny Doe</h3>
                    <p className='color-gry '>Designer</p>
                    <div>
                      <span className='color-gry '>
                        <Icon name='facebook f' />
                      </span>
                      <span style={{ marginLeft: '7px', marginRight: '7px' }} className='color-gry '>
                        <Icon name='facebook f' />
                      </span>
                      <span className='color-gry '>
                        <Icon name='facebook f' />
                      </span>
                    </div>
                  </div>
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Container>
  );
}
