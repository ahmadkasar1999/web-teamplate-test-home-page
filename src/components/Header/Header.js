import React from 'react';
import './style.css';
import { Grid } from 'semantic-ui-react';
import Navbar from '../Navbar/Navbar ';
export default function Header() {
  return (
    <>
      <Grid
        style={{
          backgroundImage: 'url(/asset/header.jpg)',
          backgroundRepeat: 'no-repeat',
          backgroundSize: 'cover',
          backgroundPosition: 'center center',
          backgroundAttachment: 'fixed',
          height: '600px',
        }}
      >
        <div className='overly'>
          <Navbar />
          <div className='headerContent'>
            <div style={{ height: 'max-content', width: '100%' }}>
              <h1 className='title'>Hello!</h1>
              <p>
                <strong className='italic-title italic-title-small'>We love our working...</strong>
              </p>
              <p>
                <strong className='italic-title italic-title-big'>And we're taking it seriously...</strong>
              </p>
            </div>
          </div>
        </div>
      </Grid>
      <div>
        <Grid>
          <Grid.Row>
            <Grid.Column width={8}>
              <div className='text-center reg-login  login'>Login</div>
            </Grid.Column>
            <Grid.Column width={8}>
              <div className='text-center reg-login register '>Register</div>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
    </>
  );
}
