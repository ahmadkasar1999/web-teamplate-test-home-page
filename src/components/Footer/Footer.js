import React from 'react';
import { Container, Divider, Grid, List, Segment, Icon } from 'semantic-ui-react';
import './style.css';
export default function Footer() {
  return (
    <Segment inverted vertical style={{ margin: '5em 0em 0em', padding: '5em 0em' }}>
      <Container textAlign='center'>
        <div className='footer-header'>
          <p style={{ paddingBottom: '0px', marginBottom: '0px' }}>LOGO</p>
          <List horizontal inverted link size='small'>
            <List.Item as='a' href='#'>
              HOME
            </List.Item>
            <List.Item as='a' href='#'>
              ABOUT US
            </List.Item>
            <List.Item as='a' href='#'>
              BLOG
            </List.Item>
            <List.Item as='a' href='#'>
              POLICY
            </List.Item>
            <List.Item as='a' href='#'>
              CONTACT
            </List.Item>
          </List>
        </div>

        <Divider inverted section />

        <Grid divided inverted stackable>
          <Grid.Column width={5} className='padding-small'>
            <p style={{ textAlign: 'start' }} className='padding-small'>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
            </p>
            <p style={{ textAlign: 'start' }} className='padding-small'>
              Design by : XpeedStudio
            </p>
          </Grid.Column>
          <Grid.Column width={3} className='box-shadow-hidden'>
            <List link inverted className='padding-small'>
              <List.Item as='a' className='flex'>
                <Icon name='chevron right' /> Support
              </List.Item>
              <List.Item as='a' className='flex'>
                <Icon name='chevron right' /> Account
              </List.Item>
              <List.Item as='a' className='flex'>
                <Icon name='chevron right' /> Product Catalog
              </List.Item>
              <List.Item as='a' className='flex'>
                <Icon name='chevron right' /> Shortcodes
              </List.Item>
            </List>
          </Grid.Column>
          <Grid.Column width={3} className='box-shadow-hidden '>
            <List link inverted className='padding-small '>
              <List.Item as='a' className='flex'>
                <Icon name='chevron right' /> Support
              </List.Item>
              <List.Item as='a' className='flex'>
                <Icon name='chevron right' /> Account
              </List.Item>
              <List.Item as='a' className='flex'>
                <Icon name='chevron right' /> Product Catalog
              </List.Item>
              <List.Item as='a' className='flex'>
                <Icon name='chevron right' /> Shortcodes
              </List.Item>
            </List>
          </Grid.Column>
          <Grid.Column width={5}>
            <List link inverted className='padding-small padding-contact'>
              <List.Item as='a' className='flex'>
                <Icon name='phone' className='m-right-5' /> 0000-000-00
              </List.Item>
              <List.Item as='a' className='flex'>
                <Icon name='envelope' className='m-right-10' /> info@gmail.com
              </List.Item>
              <List.Item as='a' className='flex'>
                Lorem ipsum dolor sit amet
              </List.Item>
              <List.Item as='a' className='flex'>
                Ipsum
              </List.Item>
            </List>
          </Grid.Column>
        </Grid>
      </Container>
    </Segment>
  );
}
