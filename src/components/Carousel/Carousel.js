import React from 'react';
import Slider from 'infinite-react-carousel';
import './style.css';
export default function Carousel() {
  const settings = {
    arrows: false,
    dots: true,
  };

  return (
    <div
      style={{
        backgroundImage: 'url(/asset/carsulphoto.jpg)',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        backgroundPosition: 'center center',
        height: '260px',
        padding: '4.8rem 2rem 0rem',
        marginTop: '50px',
      }}
      className='center-small'
    >
      <div>
        <Slider {...settings}>
          <div className='center white-color'>
            <h3>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
              <br className='hidden' /> ut labore et dolore magna aliqua.
            </h3>
            <h4>Ahmad ka</h4>
          </div>
          <div className='center white-color'>
            <h3>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt <br />
              ut labore et dolore magna aliqua.
            </h3>
            <h4>Ahmad ka</h4>{' '}
          </div>
          <div className='center white-color'>
            <h3>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
              <br /> ut labore et dolore magna aliqua.
            </h3>
            <h4>Ahmad ka</h4>{' '}
          </div>
        </Slider>
      </div>
    </div>
  );
}
