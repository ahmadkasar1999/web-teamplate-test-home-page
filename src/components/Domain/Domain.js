import React from 'react';
import { Container, Grid, Header, Label, Dropdown, Input } from 'semantic-ui-react';
import './style.css';
export default function Domain() {
  const genderOptions = [
    { key: '.com', text: '.com', value: '.com' },
    { key: '.ca', text: '.ca', value: '.ca' },
    { key: '.au', text: '.au', value: '.au' },
  ];
  return (
    <Container textAlign='center'>
      <div style={{ paddingTop: '80px' }}>
        <Header as='h3'>Would you like to buy a new domain ?</Header>
        <p style={{ color: 'rgb(159 159 159)' }}>Lorem ipsum dolor sit amet, consectetur adipiscing elit Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
        <Grid>
          <Grid.Column mobile={16} tablet={15} computer={16} verticalAlign='middle' textAlign='center'>
            <Input labelPosition='right' type='text' className='input-domain '>
              <Label basic>www.</Label>
              <input />
              <Dropdown className='drop-down-padding-small' button basic floating options={genderOptions} defaultValue='.com' placeholder='.com' />
            </Input>
          </Grid.Column>
        </Grid>
        <div className='parent-box'>
          <div className='box-width'>
            <Grid celled='internally' style={{ justifyContent: 'center' }}>
              <Grid.Row className='width-row' style={{ marginBottom: '10px' }}>
                <Grid.Column width={4} textAlign='center' verticalAlign='middle'>
                  <div style={{ padding: '20px', color: 'rgb(173 173 173)' }}>.com</div>
                </Grid.Column>
                <Grid.Column width={4} textAlign='center' verticalAlign='middle'>
                  <div style={{ padding: '20px', color: 'rgb(173 173 173)' }}> 1 year</div>
                </Grid.Column>
                <Grid.Column width={4} textAlign='center' verticalAlign='middle'>
                  <div style={{ padding: '20px', color: 'rgb(173 173 173)' }}> $9.00 USD</div>
                </Grid.Column>
                <Grid.Column width={4} textAlign='center' verticalAlign='middle'>
                  <div style={{ padding: '20px', color: 'rgb(173 173 173)' }}> Transfer Price No</div>
                </Grid.Column>
              </Grid.Row>
              <Grid.Row className='width-row padding-row box-shadow'>
                <Grid.Column width={4} textAlign='center' verticalAlign='middle' className='info'>
                  <div style={{ padding: '20px', color: 'rgb(173 173 173)' }}>.com</div>
                </Grid.Column>
                <Grid.Column width={4} textAlign='center' verticalAlign='middle' className='info'>
                  <div style={{ padding: '20px', color: 'rgb(173 173 173)' }}> 1 year</div>
                </Grid.Column>
                <Grid.Column width={4} textAlign='center' verticalAlign='middle' className='info'>
                  <div style={{ padding: '20px', color: 'rgb(173 173 173)' }}> $9.00 USD</div>
                </Grid.Column>
                <Grid.Column width={4} textAlign='center' verticalAlign='middle' className='info'>
                  <div style={{ padding: '20px', color: 'rgb(173 173 173)' }}> Transfer Price No</div>
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </div>
        </div>
      </div>
    </Container>
  );
}
