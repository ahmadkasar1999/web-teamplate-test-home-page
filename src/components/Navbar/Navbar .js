import React, { useState } from 'react';
import { Menu } from 'semantic-ui-react';
import './style.css';
import { FiMenu, FiX } from 'react-icons/fi';

export default function Navbar() {
  const [activeItem, setActiveItem] = useState('HOME');
  const handleItemClick = (name) => setActiveItem(name);
  const [open, setOpen] = useState(false);
  const handleClick = () => {
    setOpen(!open);
  };

  const closeMenu = () => {
    setOpen(false);
  };
  return (
    <nav className='navbar'>
      <Menu pointing secondary className='width-full'>
        <Menu.Menu position='left'>
          <Menu.Item name='LOGO' className='white' />
        </Menu.Menu>

        <Menu.Menu position='right' className={open ? 'nav-links active' : 'nav-links'}>
          <Menu.Item
            className='grey nav-item'
            name='HOME'
            active={activeItem === 'HOME'}
            onClick={() => {
              handleItemClick('HOME');
              closeMenu();
            }}
          />
          <Menu.Item
            className='grey nav-item'
            name='DOMAIN'
            active={activeItem === 'DOMAIN'}
            onClick={() => {
              handleItemClick('DOMAIN');
              closeMenu();
            }}
          />
          <Menu.Item
            className='grey nav-item'
            name='HOSTING'
            active={activeItem === 'HOSTING'}
            onClick={() => {
              handleItemClick('HOSTING');
              closeMenu();
            }}
          />
          <Menu.Item
            className='grey nav-item'
            name='CLIENTS'
            active={activeItem === 'CLIENTS'}
            onClick={() => {
              handleItemClick('CLIENTS');
              closeMenu();
            }}
          />
          <Menu.Item
            className='grey nav-item'
            name='BLOG'
            active={activeItem === 'BLOG'}
            onClick={() => {
              handleItemClick('BLOG');
              closeMenu();
            }}
          />
          <Menu.Item
            className='grey nav-item'
            name='SUPPORT'
            active={activeItem === 'SUPPORT'}
            onClick={() => {
              handleItemClick('SUPPORT');
              closeMenu();
            }}
          />
        </Menu.Menu>
      </Menu>
      <div onClick={handleClick} className='nav-icon '>
        {open ? <FiX /> : <FiMenu />}
      </div>
    </nav>
  );
}
