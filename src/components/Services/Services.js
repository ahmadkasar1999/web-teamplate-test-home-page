import React from 'react';
import { Container, Grid, Icon } from 'semantic-ui-react';
import './style.css';
export default function Services() {
  return (
    <Container style={{ paddingTop: '90px', paddingBottom: '40px' }}>
      <Grid>
        <Grid.Column mobile={16} tablet={8} computer={4} textAlign='center'>
          <p style={{ textAlign: '-webkit-center' }}>
            <h4 className='services-icon '>
              <Icon name='skype' size='large' />
            </h4>
          </p>
          <h5>Fast Services</h5>
          <p className='descrip-color'>
            Lorem ipsum dolor sit amet
            <br /> consectetur adipiscing elit
          </p>
        </Grid.Column>
        <Grid.Column mobile={16} tablet={8} computer={4} textAlign='center'>
          <p style={{ textAlign: '-webkit-center' }}>
            <h4 className='services-icon '>
              <Icon name='skype' size='large' />
            </h4>
          </p>
          <h5>Fast Services</h5>
          <p className='descrip-color'>
            Lorem ipsum dolor sit amet
            <br /> consectetur adipiscing elit
          </p>
        </Grid.Column>
        <Grid.Column mobile={16} tablet={8} computer={4} textAlign='center'>
          <p style={{ textAlign: '-webkit-center' }}>
            <h4 className='services-icon '>
              <Icon name='skype' size='large' />
            </h4>
          </p>
          <h5>Fast Services</h5>
          <p className='descrip-color'>
            Lorem ipsum dolor sit amet
            <br /> consectetur adipiscing elit
          </p>
        </Grid.Column>
        <Grid.Column mobile={16} tablet={8} computer={4} textAlign='center'>
          <p style={{ textAlign: '-webkit-center' }}>
            <h4 className='services-icon '>
              <Icon name='skype' size='large' />
            </h4>
          </p>
          <h5>Fast Services</h5>
          <p className='descrip-color'>
            Lorem ipsum dolor sit amet
            <br /> consectetur adipiscing elit
          </p>
        </Grid.Column>
      </Grid>
    </Container>
  );
}
